import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.taglibs.standard.tag.common.core.NullAttributeException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class AdFilter {
	
	private Map<String, HashMap<String,Object>> adProviders;
	private String[] allAdProviders = {"Adx", "AdMob", "Unity Ads", "Facebook"};
	private Map<String, String[]> bannedAdProvidersPerCountry;
	private Map<String, String[]> bannedAdProvidersPerOSVer;
	
	
	/*adProviders structure:
		adProviders - Banner - list of adProviders (adProvider name, Object - assuming that the SDK has a custom object from which you call functions)
					- Interstitial - list of adProviders (adProvider name, Object)
					- RewardedVideo - list of adProviders (adProvider name, Object)
	
	*/
	
	public AdFilter() {
		
		HashMap<String, Object> testMap = new HashMap<String,Object>() {{
			put("Adx", null);
			put("AdMob", null);
			put("Unity Ads", null);
			put("Facebook", null);
		}};
		
		HashMap<String, Object> testMapNoAdMob = new HashMap<String,Object>() {{
			put("Adx", null);
			put("Unity Ads", null);
			put("Facebook", null);
		}};
		
		
		
		this.adProviders = new HashMap<String, HashMap<String,Object>>();
		this.adProviders.put("Banner", testMap);
		this.adProviders.put("Interstitial", testMapNoAdMob);
		this.adProviders.put("RewardedVideo", testMap);
		
		
		
		this.bannedAdProvidersPerCountry = new HashMap<String,String[]>();
		this.bannedAdProvidersPerOSVer = new HashMap<String,String[]>();
		
		this.bannedAdProvidersPerCountry.put("CN", new String[]{"Facebook"});
		this.bannedAdProvidersPerOSVer.put("9", new String[]{"AdMob"});
	}
	
	public AdFilter(Map<String, HashMap<String,Object>> adProviders ) {
		this.adProviders = adProviders;
		
		
	}
	
	private Map<String, HashMap<String, Object>> filterAds(Map<String,String[]> clientProperties){
		Map<String, HashMap<String, Object>> filteredAds = this.adProviders;
		
		try {
			
			filteredAds.forEach((k,v) -> {
				//checks each hashmap (banner, interstitial, rewardedvideo) and filters them.
				
				
				filterBasedOnCountry(clientProperties.get("countryCode")[0], v);
				filterBasedOnOSVer(clientProperties.get("osVersion")[0], v);
				
				for (String aP : allAdProviders) {
					//checks for any missing ad providers
					if(!v.containsKey(aP)) {
						v.put(aP + "-OptOut", null);
					}else {
						v.remove(aP + "-OptOut");
					}
				}
				
				
				
			});				
		
		}catch(Exception e) {
			
		}
		
		return this.adProviders;
	}
	
	public JsonObject fetchAdProviders(Map<String,String[]> clientProperties) {
		Map<String, HashMap<String,Object>> adProviders = filterAds(clientProperties);
	
		Gson G = new Gson();
		
		JsonObject JO = new JsonObject();
		
		
		Iterator<Entry<String, HashMap<String, Object>>> i = adProviders.entrySet().iterator();
		while(i.hasNext()) {
			Map.Entry<String, HashMap<String,Object>> entry = (Map.Entry)i.next();
			JO.add(entry.getKey(), G.toJsonTree(entry.getValue()));
		}
		
		
		System.out.print("Json: " + JO.toString());
		
		return JO;
	}
	
	private void filterBasedOnCountry(String countryCode, HashMap<String,Object> adProviders){		
		if(!countryCode.isEmpty()) {
			for(String adP : bannedAdProvidersPerCountry.get(countryCode)){
				adProviders.remove(adP);
			}
		}else {
			//throw null exception
		}
	}
	
	private void filterBasedOnOSVer(String osVersion, HashMap<String,Object> adProviders) {
		if(!osVersion.isEmpty()) {
			for(String adP : bannedAdProvidersPerOSVer.get(osVersion)) {
				adProviders.remove(adP);
			}
		}else {
			//throw null exception
		}
	}
	
	
	
}
