import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.bigquery.model.JsonObject;
import com.google.gson.annotations.JsonAdapter;
import com.google.protobuf.util.JsonFormat;




@WebServlet(
    name = "HelloAppEngine",
    urlPatterns = {"/hello"}
)
public class AdMediationEngine extends HttpServlet {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static AdFilter aF = null;
	
@Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
      throws IOException {
	
	Map<String, String[]> parMap = request.getParameterMap();
	
	aF = new AdFilter();
	
	com.google.gson.JsonObject a = aF.fetchAdProviders(parMap);
	
    response.setContentType("text/plain");
    response.setCharacterEncoding("UTF-8");
  
   
    response.getWriter().print("Hello App Engine!\r\n url pars: " + parMap.toString() + " JSON object: " + a.toString());

  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) 
	 throws IOException {
	  
	  
	  
	  
	  aF = new AdFilter();
	  
  }

}


